package com.mercury.mercuryadmin

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.mercury.mercuryadmin.models.Order
import com.mercury.mercuryadmin.utils.commons.TAG_PRODUCT_DETAILS
import kotlinx.android.synthetic.main.activity_detail.*


class DetailActivity : BaseActivity(), View.OnClickListener {

    private var product: Order? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        handleBundle(intent)

    }

    private fun handleBundle(intent: Intent?) {
        if (intent?.extras != null) {
            val productDetails = intent.extras
            if (productDetails?.containsKey(TAG_PRODUCT_DETAILS)!!) {
                product = Gson().fromJson<Order>(productDetails.getString(TAG_PRODUCT_DETAILS).toString(), Order::class.java)
                initView()
            }
        }
    }

    private fun initView() {
        tvTitle.text = product?.title.toString().capitalize()
        tvQuantity.text = product?.quantity.toString()
        tvBill.text = product?.price.toString()
        tvDescription.text = product?.description
        tvCustomerName.text = product?.customerName
        tvCustomerPhone.text = product?.customerPhone
        tvCustomerAddress.text = product?.customerAddress

        if (product?.status==1){
            btnComplete.isEnabled = false
        }

        btnComplete.setOnClickListener(this)
        btnCall.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btnCall -> {
                callCustomer()
            }
            R.id.btnComplete -> {
                progressDialog.show()
                val order: Order = Order()
                fireDbRef = database.getReference("Orders")
                order.status = 1
                fireDbRef.child(product?.orderId!!.toString()).setValue(order).addOnCompleteListener {
                    progressDialog.dismiss()
                    if (it.isSuccessful) {
                        Toast.makeText(this, "Successfully updated", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    private fun callCustomer() {
        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + product?.customerPhone))
        startActivity(intent)
    }
}
