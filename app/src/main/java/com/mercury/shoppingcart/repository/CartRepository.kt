package com.mercury.shoppingcart.repository

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.mercury.shoppingcart.db.CartProductDatabase
import com.mercury.shoppingcart.network.model.CartProduct

class CartRepository(private val context: Context) {

    private val DB_NAME = "cartDb"
    private val noteDatabase = Room.databaseBuilder(context, CartProductDatabase::class.java, DB_NAME).allowMainThreadQueries().build()

    fun insertTask(cartItem: CartProduct?) = noteDatabase.daoAccess().insertTask(cartItem)

    fun deleteTask(id: Int) {
        val task = getTask(id)
        if (task != null) {
            noteDatabase.daoAccess().deleteTask(task.value)
        }
    }

    fun deleteTask(note: CartProduct?) {
        noteDatabase.daoAccess().deleteTask(note)
    }

    fun getTask(id: Int): LiveData<CartProduct?>? {
        return noteDatabase.daoAccess().getTask(id)
    }

    val tasks: LiveData<List<CartProduct?>?>?
        get() = noteDatabase.daoAccess().fetchAllTasks()

    fun getByType(id: Int): LiveData<List<CartProduct?>?>? {
        return noteDatabase.daoAccess().getByType(id)
    }
}