package com.mercury.mercuryadmin.models

import com.google.gson.annotations.SerializedName

data class Products(
                    @SerializedName("title")
                    var title: String? = "",
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("imgUrl")
                    var imgUrl: String? = "",
                    @SerializedName("price")
                    var price: Int = 0,
                    @SerializedName("categoryId")
                    var categoryId: Int = 0)