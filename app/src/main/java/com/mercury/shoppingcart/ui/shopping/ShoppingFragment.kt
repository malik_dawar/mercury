package com.mercury.shoppingcart.ui.shopping

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.mercury.shoppingcart.R
import com.mercury.shoppingcart.adapters.CartAdapter
import com.mercury.shoppingcart.enums.CartType
import com.mercury.shoppingcart.network.model.CartProduct
import com.mercury.shoppingcart.network.model.Products
import com.mercury.shoppingcart.repository.CartRepository
import com.mercury.shoppingcart.ui.BaseFragment
import com.mercury.shoppingcart.ui.main.CheckoutActivity
import com.mercury.shoppingcart.utils.commons.TAG_PRODUCT_DETAILS
import com.mercury.shoppingcart.utils.commons.TAG_PRODUCT_QUANTITY
import kotlinx.android.synthetic.main.fragment_shopping.*

class ShoppingFragment : BaseFragment(), CartAdapter.CartProductSelector {

    private lateinit var cartAdapter: CartAdapter
    private lateinit var cartRepository: CartRepository

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_shopping, container, false)
        cartRepository = context?.let { CartRepository(it) }!!

        progressDialog.dismiss()

        cartRepository.getByType(CartType.Cart.id)?.observe(viewLifecycleOwner, Observer {
            progressDialog.dismiss()

            cartAdapter = CartAdapter(it, this)
            recycler_view?.apply {
                layoutManager = GridLayoutManager(context, 2)
                itemAnimator = DefaultItemAnimator()
                adapter = cartAdapter
            }
        })
        return root
    }

    override fun getSelectedProduct(productItem: CartProduct) {
        val product: Products = Products()
        product.title = productItem.title
        product.description = productItem.description
        product.imgUrl = productItem.imgUrl
        product.price = productItem.price
        product.categoryId = productItem.categoryId

        val intent = Intent(context, CheckoutActivity::class.java)
        intent.putExtra(TAG_PRODUCT_DETAILS, Gson().toJson(product).toString())
        intent.putExtra(TAG_PRODUCT_QUANTITY, productItem.quantity)
        startActivity(intent)

    }

    override fun onItemLongPress(cartItem: CartProduct) {
        cartRepository.deleteTask(cartItem)
    }
}