package com.mercury.shoppingcart.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mercury.shoppingcart.network.model.CartProduct

@Dao
interface DaoAccess {
    @Insert
    fun insertTask(note: CartProduct?): Long

    @Query("SELECT * FROM CartProduct")
    fun fetchAllTasks(): LiveData<List<CartProduct?>?>?

    @Query("SELECT * FROM CartProduct WHERE id =:taskId")
    fun getTask(taskId: Int): LiveData<CartProduct?>?

    @Query("SELECT * FROM CartProduct WHERE typeId =:taskId")
    fun getByType(taskId: Int): LiveData<List<CartProduct?>?>?

    @Update
    fun updateTask(note: CartProduct?)

    @Delete
    fun deleteTask(note: CartProduct?)
}