package com.mercury.shoppingcart.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.mercury.shoppingcart.R
import com.mercury.shoppingcart.adapters.SliderAdapterExample.SliderAdapterVH
import com.smarteist.autoimageslider.SliderViewAdapter

internal class SliderAdapterExample : SliderViewAdapter<SliderAdapterVH>() {
    override fun onCreateViewHolder(parent: ViewGroup): SliderAdapterVH {
        val inflate = LayoutInflater.from(parent.context).inflate(R.layout.image_slider_layout_item, null)
        return SliderAdapterVH(inflate)
    }

    override fun onBindViewHolder(viewHolder: SliderAdapterVH, position: Int) { // viewHolder.textViewDescription.setText("This is slider item " + position);
        when (position) {
            1 -> viewHolder.imageViewBackground.setImageResource(R.drawable.iv_promotional_gateway)
            2 -> viewHolder.imageViewBackground.setImageResource(R.drawable.slider3)
            else -> viewHolder.imageViewBackground.setImageResource(R.drawable.iv_poss_producation)//iv_signatures_solution
        }
    }

    override fun getCount(): Int { //slider view count could be dynamic size
        return 3
    }

    internal inner class SliderAdapterVH(var itemView: View) : ViewHolder(itemView) {
        var imageViewBackground: ImageView = itemView.findViewById(R.id.iv_auto_image_slider)
        var textViewDescription: TextView = itemView.findViewById(R.id.tv_auto_image_slider)

    }
}