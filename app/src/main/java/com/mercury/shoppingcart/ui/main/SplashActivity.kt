package com.mercury.shoppingcart.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.mercury.shoppingcart.R

class SplashActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            startActivity(Intent(this@SplashActivity, BottomNavActivity::class.java))
            finish()
        }, 1500)
    }
}
