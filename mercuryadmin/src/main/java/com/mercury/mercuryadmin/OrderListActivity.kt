package com.mercury.mercuryadmin

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import com.mercury.mercuryadmin.adapters.OrderAdapter
import com.mercury.mercuryadmin.models.Order
import com.mercury.mercuryadmin.utils.commons.TAG_PRODUCT_DETAILS
import kotlinx.android.synthetic.main.activity_order_list.*

class OrderListActivity : BaseActivity(), OrderAdapter.ProductSelector {

    var ordersList: ArrayList<Order>? = ArrayList()
    lateinit var productAdapter: OrderAdapter
    lateinit var valueEventListener: ValueEventListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_list)
        fireDbRef = database.getReference("Orders")

        fetchDataFromFireBase()

    }

    private fun fetchDataFromFireBase() {
        progressDialog.show()


        valueEventListener = fireDbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                progressDialog.dismiss()
                for (dataSnapshot in dataSnapshot.children) {
                    val products: Order? = dataSnapshot.getValue(Order::class.java)
                    products?.orderId = dataSnapshot.key
                    if (products != null) {
                        ordersList?.add(products)
                    }
                }

                productAdapter = OrderAdapter(ordersList, this@OrderListActivity)
                recycler_view.apply {
                    layoutManager = GridLayoutManager(this@OrderListActivity, 1)
                    itemAnimator = DefaultItemAnimator()
                    adapter = productAdapter
                }
                fireDbRef.removeEventListener(valueEventListener)

            }

            override fun onCancelled(error: DatabaseError) { // Failed to read value
                progressDialog.dismiss()
                Log.w(ContentValues.TAG, "Failed to read value.", error.toException())
            }
        })
    }

    override fun getSelectedProduct(productItem: Order) {
        val intent = Intent(this@OrderListActivity, DetailActivity::class.java)
        intent.putExtra(TAG_PRODUCT_DETAILS, Gson().toJson(productItem).toString())
        startActivity(intent)
    }
}
