package com.mercury.shoppingcart.ui.home

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.mercury.shoppingcart.R
import com.mercury.shoppingcart.adapters.CategoriesAdapter
import com.mercury.shoppingcart.adapters.SliderAdapterExample
import com.mercury.shoppingcart.enums.CategoriesEnum
import com.mercury.shoppingcart.ui.categoryDetail.CategoryItemsFragment
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import kotlinx.android.synthetic.main.fragment_dahboard.*
import com.mercury.shoppingcart.network.model.Categories
import com.mercury.shoppingcart.ui.BaseFragment

class DashboardFragment : BaseFragment(), CategoriesAdapter.CategorySelector {

    private var categoryList: ArrayList<Categories>? = ArrayList()
    lateinit var categoriesAdapter: CategoriesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_dahboard, container, false)

        val sliderView: SliderView = root.findViewById(R.id.imageSliderF)
        val adapter = SliderAdapterExample()
        sliderView.sliderAdapter = adapter
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM)
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        sliderView.indicatorSelectedColor = Color.WHITE
        sliderView.indicatorUnselectedColor = Color.GRAY
        sliderView.scrollTimeInSec = 4

        sliderView.startAutoCycle()

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoryList?.add(Categories(CategoriesEnum.PosProduction.title, R.drawable.slider5 ,CategoriesEnum.PosProduction.id))
        categoryList?.add(Categories(CategoriesEnum.BusinessStationery.title, R.drawable.slider4 ,CategoriesEnum.BusinessStationery.id))
        categoryList?.add(Categories(CategoriesEnum.PromotionalGateway.title, R.drawable.slider2 ,CategoriesEnum.PromotionalGateway.id))
        categoryList?.add(Categories(CategoriesEnum.SignatureSolution.title, R.drawable.slider1 ,CategoriesEnum.SignatureSolution.id))


        categoriesAdapter = CategoriesAdapter(categoryList, this)
        recycler_view?.apply {
            layoutManager = GridLayoutManager(context, 1)
            itemAnimator = DefaultItemAnimator()
            adapter = categoriesAdapter
        }
    }

    private fun moveT0Main(fragment: Fragment) {
        val fragmentTransition = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransition.replace(R.id.nav_host_fragment, fragment)
        fragmentTransition.commitAllowingStateLoss()
    }

    override fun getSelectedCategory(categories: Categories?) {
        moveT0Main(CategoryItemsFragment.newInstance(categoryId = categories?.categoryId!!))
    }
}