package com.mercury.shoppingcart.network.model

import com.google.gson.annotations.SerializedName

data class Categories(
        @SerializedName("title")
        var title: String? = "",
        @SerializedName("imgUrl")
        var imgUrl: Int? = null,
        @SerializedName("categoryId")
        var categoryId: Int? = null)