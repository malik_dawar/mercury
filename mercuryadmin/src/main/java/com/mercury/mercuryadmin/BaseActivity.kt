package com.mercury.mercuryadmin

import android.app.Activity
import android.app.ProgressDialog
import android.os.Bundle
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

abstract class BaseActivity : Activity() {
    protected lateinit var progressDialog: ProgressDialog
    protected var database: FirebaseDatabase = FirebaseDatabase.getInstance()
    protected  lateinit var fireDbRef: DatabaseReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Initialize Firebase
        fireDbRef = database.getReference("Products")

        progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Processing")
        progressDialog.setMessage("Please wait")
    }
}