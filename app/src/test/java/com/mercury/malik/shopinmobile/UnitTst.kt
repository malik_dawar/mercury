package com.mercury.malik.shopinmobile

import org.junit.Assert.assertArrayEquals
import org.junit.Test

class UnitTst {

    @Test
    fun insertionSort() {
        val data = arrayOf(4, 3, 2, 10, 12, 1, 5, 6, 10)

        data.forEachIndexed { index, item ->
            if (index == 0)
                return@forEachIndexed

            var item1Index = index - 1
            while (item1Index >= 0 && data[item1Index] > item){ // if 1st item is bigger than 2nd than replace 2nd with 1st.
                data[item1Index + 1] = data[item1Index]    // place 1st item at 2nd.
                item1Index--
            }
            data[item1Index + 1] = item          // place 2nd item at 1st.
        }
        println(data.contentToString())
        assertArrayEquals(arrayOf(1, 2, 3, 4, 5, 6, 10, 10, 12), data)

        //complexity = n^2
    }
}