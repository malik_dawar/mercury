package com.mercury.shoppingcart.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mercury.shoppingcart.dao.DaoAccess
import com.mercury.shoppingcart.network.model.CartProduct

@Database(entities = [CartProduct::class], version = 1, exportSchema = false)
abstract class CartProductDatabase : RoomDatabase() {
    abstract fun daoAccess(): DaoAccess
}