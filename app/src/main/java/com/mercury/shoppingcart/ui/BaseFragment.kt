package com.mercury.shoppingcart.ui

import android.app.ProgressDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.kaopiz.kprogresshud.KProgressHUD
import com.mercury.shoppingcart.pref.PrefManager

abstract class BaseFragment : Fragment() {
    protected lateinit var progressDialog: KProgressHUD
    protected var database: FirebaseDatabase = FirebaseDatabase.getInstance()
    protected  lateinit var fireDbRef: DatabaseReference
    protected lateinit var prefManager: PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Initialize Firebase
        fireDbRef = database.getReference("Products")

        prefManager = PrefManager(context!!)

        progressDialog =  KProgressHUD.create(activity)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
    }
}