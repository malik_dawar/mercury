package com.mercury.shoppingcart.network.model

import com.google.gson.annotations.SerializedName

data class Order(
        @SerializedName("customerName")
        var customerName: String? = "",
        @SerializedName("customerPhone")
        var customerPhone: String? = "",
        @SerializedName("customerAddress")
        var customerAddress: String? = "",
        @SerializedName("title")
        var title: String? = "",
        @SerializedName("description")
        var description: String? = "",
        @SerializedName("quantity")
        var quantity: Int = 0,
        @SerializedName("bill")
        var price: Double = 0.0,
        @SerializedName("status")
        var status: Int = 0)