package com.mercury.shoppingcart.utils;

public class commons {

    public static final String TAG_OBJECTID = "objectId";
    public static final String TAG_EMAIL = "email";
    public static final String TAG_NAME = "name";
    public static final String TAG_NUMBER = "number";
    public static final String TAG_ADDRESS = "address";
    public static final String TAG_PASSWORD = "password";
    public static final String TAG_USERSESSION = "UserSession";
    public static final String TAG_IMAGE = "Image";
    public static final String TAG_ABOUT = "discription";
    public static final String TAG_PRODUCT_DETAILS = "productDetails";
    public static final String TAG_PRODUCT_QUANTITY = "quantity";
    public static final String TAG_CATEGORY_ID = "categoriesId";


    public static final String TAG_OK_GET = "200";
    public static final String TAG_ERROR_UNAUTHORIZED = "401";
    public static final String TAG_ERROR = "400";
    public static final String TAG_NO_RESPONSE = "500";
    public static final String TAG_OK_POST = "201";

}
                                            