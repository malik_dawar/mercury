package com.mercury.shoppingcart.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mercury.shoppingcart.R
import com.mercury.shoppingcart.network.model.CartProduct
import com.mercury.shoppingcart.network.model.Products

class CartAdapter(private val dataList: List<CartProduct?>?, private val productSelector: CartProductSelector) : RecyclerView.Adapter<CartAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var itemImgView = view.findViewById<View>(R.id.itemImgView) as ImageView
        var itemTvPrice = view.findViewById<View>(R.id.itemTvPrice) as TextView
        var itemTvName = view.findViewById<View>(R.id.itemTvName) as TextView
        var cardItem = view.findViewById<View>(R.id.cardItem) as CardView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.ui_cart_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val distItem = dataList?.get(position)
        holder.itemTvName.text = distItem?.title!!.capitalize()
        val cost = distItem.price
        if (cost == 0) {
            holder.itemTvPrice.visibility = View.GONE
        } else {
            holder.itemTvPrice.visibility = View.VISIBLE
            holder.itemTvPrice.text = "only $cost AED"
        }

        Glide.with(holder.itemImgView.context).load(distItem.imgUrl).into(holder.itemImgView)

        holder.cardItem.setOnClickListener {
            productSelector.getSelectedProduct(distItem)
        }
        
        holder.cardItem.setOnLongClickListener {
            productSelector.onItemLongPress(distItem)
            true
        }
    }

    override fun getItemCount(): Int {
        return dataList?.size!!
    }

    interface CartProductSelector {
        fun getSelectedProduct(productItem: CartProduct)
        fun onItemLongPress(cartItem: CartProduct)
    }
}