package com.mercury.shoppingcart.ui.categoryDetail

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import com.mercury.shoppingcart.R
import com.mercury.shoppingcart.adapters.ProductAdapter
import com.mercury.shoppingcart.network.model.Products
import com.mercury.shoppingcart.ui.BaseFragment
import com.mercury.shoppingcart.ui.main.DetailActivity
import com.mercury.shoppingcart.utils.commons
import com.mercury.shoppingcart.utils.commons.TAG_CATEGORY_ID
import kotlinx.android.synthetic.main.fragment_category_detail.*

class CategoryItemsFragment : BaseFragment(), ProductAdapter.ProductSelector {

    private var productList: ArrayList<Products>? = ArrayList()
    private lateinit var productAdapter: ProductAdapter
    private lateinit var valueEventListener: ValueEventListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_category_detail, container, false)
        return root
    }

    private fun fetchDataFromFireBase() {
        progressDialog.show()

        valueEventListener = fireDbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                progressDialog.dismiss()
                for (fireBaseDataSnapshot in dataSnapshot.children) {
                    val products: Products? = fireBaseDataSnapshot.getValue(Products::class.java)
                    if (products != null) {
                        //todo categoryId
                        if (products.categoryId == categoryId) {
                            productList?.add(products)
                        }
                    }
                }

                productAdapter = ProductAdapter(productList, this@CategoryItemsFragment)
                recycler_view?.apply {
                    layoutManager = GridLayoutManager(context, 2)
                    itemAnimator = DefaultItemAnimator()
                    adapter = productAdapter
                }
                fireDbRef.removeEventListener(valueEventListener)

            }

            override fun onCancelled(error: DatabaseError) { // Failed to read value
                progressDialog.dismiss()
                Log.w(ContentValues.TAG, "Failed to read value.", error.toException())
            }
        })
    }

    override fun getSelectedProduct(productItem: Products) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra(commons.TAG_PRODUCT_DETAILS, Gson().toJson(productItem).toString())
        startActivity(intent)
    }

    private var categoryId: Int = 0


    companion object {
        fun newInstance(categoryId: Int) = CategoryItemsFragment().apply {
            arguments = Bundle().apply {
                putInt(TAG_CATEGORY_ID, categoryId)
            }
        }
    }

    private fun handleBundle() {

        arguments?.apply {
            if (containsKey(TAG_CATEGORY_ID)) {
                categoryId = getInt(TAG_CATEGORY_ID)

                fetchDataFromFireBase()

                pullToRefresh.setOnRefreshListener {
                    productList?.clear()
                    fetchDataFromFireBase()
                    pullToRefresh.isRefreshing = false
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        handleBundle()
    }
}