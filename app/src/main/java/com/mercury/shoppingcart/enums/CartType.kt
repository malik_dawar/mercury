package com.mercury.shoppingcart.enums

enum class CartType(val id : Int)  {

    Cart(0),
    Favourite(1),

}