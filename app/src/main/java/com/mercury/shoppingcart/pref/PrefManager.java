package com.mercury.shoppingcart.pref;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import static com.mercury.shoppingcart.utils.commons.TAG_USERSESSION;


/**
 * Class for Shared Preference
 */
public class PrefManager {

    Context context;
    public PrefManager(Context context) {
        this.context = context;
    }

    public void putStringPref(String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("SessionDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        Log.d("sharedPreferences",key+"\t"+value);
        editor.commit();
    }

    public String getString(String key, String defaultKey){
        SharedPreferences sharedPreferences = context.getSharedPreferences("SessionDetails", Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, defaultKey);
    }



    public boolean isUserLogedOut() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("SessionDetails", Context.MODE_PRIVATE);

        return sharedPreferences.getString(TAG_USERSESSION, "").isEmpty();

    }

    public void deleteSpec(String key) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("SessionDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sharedPreferences.edit();
        ed.remove(key);
        ed.apply();
    }

    public void logoutsession(){
        SharedPreferences sharedPreferences = context.getSharedPreferences("SessionDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    public void saveSessionDetails(String username, String password) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("SessionDetails", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TAG_USERSESSION, username+password);
        editor.commit();
    }
}
