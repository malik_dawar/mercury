package com.mercury.shoppingcart.ui.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.mercury.shoppingcart.R
import com.mercury.shoppingcart.enums.CartType
import com.mercury.shoppingcart.utils.commons.TAG_PRODUCT_DETAILS
import com.mercury.shoppingcart.utils.commons.TAG_PRODUCT_QUANTITY
import com.mercury.shoppingcart.network.model.CartProduct
import com.mercury.shoppingcart.network.model.Products
import com.mercury.shoppingcart.repository.CartRepository
import com.mercury.shoppingcart.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_detail.*


class DetailActivity : BaseActivity(), View.OnClickListener {

    private var product: Products? = null
    private var itemQuantity: Int = 1
    private lateinit var cartRepository: CartRepository
    private lateinit var onSuccess: (Boolean) -> Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val attrs = window.attributes
        attrs.flags = attrs.flags or WindowManager.LayoutParams.FLAG_FULLSCREEN
        window.attributes = attrs
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)
        cartRepository = CartRepository(this@DetailActivity)
        handleBundle(intent)


    }

    private fun handleBundle(intent: Intent?) {
        if (intent?.extras != null) {
            val productDetails = intent.extras
            if (productDetails?.containsKey(TAG_PRODUCT_DETAILS)!!) {
                product = Gson().fromJson<Products>(productDetails.getString(TAG_PRODUCT_DETAILS).toString(), Products::class.java)

                Log.d("productItemDetails", "$product")
                initView()
            }
        }
    }

    private fun initView() {
        fireDbRef = database.getReference("Cart")

        Glide.with(this).load(product?.imgUrl).into(itemImgView)
        val cost = product?.price
        if (cost == 0) {
            itemTvPrice.visibility = View.GONE
            itemTvTotal.visibility = View.GONE
        } else {
            itemTvPrice.visibility = View.VISIBLE
            itemTvTotal.visibility = View.VISIBLE
            itemTvPrice.text = cost.toString() + " AED"
        }
        itemTvName.text = product?.title!!.capitalize()
        itemTvDisp.text = product?.description
        supportActionBar?.title = product?.title!!.capitalize()

        updateCartBill()

        btnAddCart.setOnClickListener(this)
        btnSubCart.setOnClickListener(this)
        btnCheckout.setOnClickListener(this)
        btnAddToCart.setOnClickListener(this)
        itemImgFav.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnAddCart -> {
                if (itemQuantity < 50) {
                    itemQuantity++
                    updateCartBill()
                }
            }
            R.id.btnSubCart -> {
                if (itemQuantity > 1) {
                    itemQuantity--
                    updateCartBill()
                }
            }
            R.id.btnAddToCart -> {
                saveInDB(CartType.Cart.id) {
                    Toast.makeText(view.context, "added to cart", Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
            R.id.btnCheckout -> {
                if (product != null) {
                    val intent = Intent(this, CheckoutActivity::class.java)
                    intent.putExtra(TAG_PRODUCT_DETAILS, Gson().toJson(product).toString())
                    intent.putExtra(TAG_PRODUCT_QUANTITY, itemQuantity)
                    startActivity(intent)
                    finish()
                }
            }
            R.id.itemImgFav -> {
                saveInDB(CartType.Favourite.id) {
                    itemImgFav.setImageResource(R.drawable.ic_favorite_red)
                    Toast.makeText(view.context, "added to Favourite", Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
        }
    }

    private fun updateCartBill() {
        itemTvQuantity.text = itemQuantity.toString()
        itemTvTotal.text = "${(product!!.price * itemQuantity)} AED"
    }

    private fun saveInDB(cartType: Int, onSucces: ((Boolean) -> Unit)) {
        this.onSuccess = onSucces

        val cartProduct = CartProduct()
        cartProduct.title = product?.title
        cartProduct.description = product?.description
        cartProduct.imgUrl = product?.imgUrl
        cartProduct.price = product!!.price
        cartProduct.categoryId = product!!.categoryId
        cartProduct.quantity = itemQuantity
        cartProduct.typeId = cartType

        if (cartRepository.insertTask(cartProduct) != -1L) {
            onSuccess(true)
        } else {
            onSuccess(false)
        }
    }
}
