package com.mercury.shoppingcart

import android.app.Application
import android.content.Context
import com.androidnetworking.AndroidNetworking

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        AndroidNetworking.initialize(applicationContext)

    }

    companion object {
        var instance: App? = null
        fun getAppContext(): Context {
            return instance as Context
        }
    }

}