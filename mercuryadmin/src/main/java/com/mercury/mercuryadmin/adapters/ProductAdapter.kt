package com.mercury.mercuryadmin.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.mercury.mercuryadmin.R
import com.mercury.mercuryadmin.models.Order

class OrderAdapter(private val dataList: ArrayList<Order>?, private val productSelector: ProductSelector) : RecyclerView.Adapter<OrderAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var tvTitle = view.findViewById<View>(R.id.tvTitle) as TextView
        var tvQuantity = view.findViewById<View>(R.id.tvQuantity) as TextView
        var tvBill = view.findViewById<View>(R.id.tvBill) as TextView
        var cardItem = view.findViewById<View>(R.id.cardItem) as CardView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.ui_product_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val distItem = dataList?.get(position)
        holder.tvTitle.text = distItem?.title!!.capitalize()
        holder.tvQuantity.text = distItem.quantity.toString()
        holder.tvBill.text = "only ${distItem?.price.toString()} AED"

        holder.cardItem.setOnClickListener {
            productSelector.getSelectedProduct(distItem)
        }
    }

    override fun getItemCount(): Int {
        return dataList?.size!!
    }

    interface ProductSelector {
        fun getSelectedProduct(productItem: Order)
    }
}