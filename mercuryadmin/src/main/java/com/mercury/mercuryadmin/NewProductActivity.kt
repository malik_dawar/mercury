package com.mercury.mercuryadmin

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import com.mercury.mercuryadmin.models.Order
import com.mercury.mercuryadmin.models.Products
import kotlinx.android.synthetic.main.activity_new_product.*

class NewProductActivity : BaseActivity() {

    var product: Products = Products()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_product)
        initView()
    }

    private fun initView() {
        btnCheckout.setOnClickListener {
            saveProductItem()
        }

        tvOrders.setOnClickListener {
            startActivity(Intent(this, OrderListActivity::class.java))
        }
    }

    private fun saveProductItem() {

        val title = edTitle.text.toString()
        val url = edUrl.text.toString()
        val price = edPrice.text.toString()
        val description = edDescription.text.toString()

        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(price) && !TextUtils.isEmpty(description)) {
            progressDialog.show()

            product.title = title
            product.imgUrl = url
            product.price = price.toInt()
            product.description = description
            product.categoryId = spCategory.selectedItemPosition

            fireDbRef.push().setValue(product).addOnCompleteListener {
                progressDialog.dismiss()
                if (it.isSuccessful) {
                    Toast.makeText(this, "Successfully Saved", Toast.LENGTH_LONG).show()
                    edTitle.text = null
                    edUrl.text = null
                    edPrice.text = null
                    edDescription.text = null
                } else {
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
                }
            }

        } else {
            Toast.makeText(this@NewProductActivity, getString(R.string.validation), Toast.LENGTH_LONG).show()
            return
        }
    }
}
