package com.mercury.shoppingcart.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mercury.shoppingcart.R
import com.mercury.shoppingcart.network.model.Categories

class CategoriesAdapter(private val dataList: ArrayList<Categories>?, private val categorySelector: CategorySelector) : RecyclerView.Adapter<CategoriesAdapter.MyViewHolder>() {

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var layoutCategory = view.findViewById<View>(R.id.layoutCategory) as RelativeLayout
        var imgCategory = view.findViewById<View>(R.id.imgCategory) as ImageView
        var tvCategory = view.findViewById<View>(R.id.tvCategory) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_catagory_ui, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val distItem = dataList?.get(position)

        holder.imgCategory.setImageResource(distItem?.imgUrl!!)
        holder.tvCategory.visibility = View.GONE
        holder.layoutCategory.setOnClickListener {
            categorySelector.getSelectedCategory(distItem)
        }
    }

    override fun getItemCount(): Int {
        return dataList?.size!!
    }

    interface CategorySelector {
        fun getSelectedCategory(categories: Categories?)
    }
}