package com.mercury.shoppingcart.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.mercury.shoppingcart.R
import com.mercury.shoppingcart.utils.commons
import com.mercury.shoppingcart.network.model.Order
import com.mercury.shoppingcart.network.model.Products
import com.mercury.shoppingcart.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_checkout.*

class CheckoutActivity : BaseActivity() {

    private var product: Products? = null
    private var itemQuantity = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)
        fireDbRef = database.getReference("Orders")
        handleBundle(intent)
    }

    private fun handleBundle(intent: Intent?) {
        if (intent?.extras != null) {
            val productDetails = intent.extras
            if (productDetails?.containsKey(commons.TAG_PRODUCT_DETAILS)!!) {
                product = Gson().fromJson<Products>(productDetails.getString(commons.TAG_PRODUCT_DETAILS).toString(), Products::class.java)
                itemQuantity = productDetails.getInt(commons.TAG_PRODUCT_QUANTITY)
                initView()
            }
        }
    }

    private fun initView() {
        if (product!!.price == 0){
            layoutBill.visibility = View.GONE
        }

        tvTitle.text = product?.title!!.capitalize()
        tvQuantity.text = "$itemQuantity"
        tvDetails.text = product?.description
        tvBill.text = "${(product!!.price * itemQuantity)} AED"

        btnCheckout.setOnClickListener {
            placeOrder()
        }
    }

    private fun placeOrder() {
        val customerName = edName.text.toString()
        val customerPhone = edPhone.text.toString()
        val customerAddress = edDescription.text.toString()

        if (customerName.isNullOrEmpty()) {
            Toast.makeText(this, "Name is required", Toast.LENGTH_LONG).show()
            return
        }
        if (customerPhone.isNullOrEmpty()) {
            Toast.makeText(this, "Phone number is required", Toast.LENGTH_LONG).show()
            return
        }
        if (customerAddress.isNullOrEmpty()) {
            Toast.makeText(this, "Address is required", Toast.LENGTH_LONG).show()
            return
        } else {

            progressDialog.show()

            val order: Order = Order()
            order.customerName = edName.text.toString()
            order.customerPhone = edPhone.text.toString()
            order.customerAddress = edDescription.text.toString()
            order.title = product?.title
            order.description = product?.description
            order.price = (product!!.price * itemQuantity).toDouble()
            order.quantity = itemQuantity
            fireDbRef.push().setValue(order).addOnCompleteListener {
                progressDialog.dismiss()
                if(it.isSuccessful){
                    Toast.makeText(this, "Order has been successfully placed", Toast.LENGTH_LONG).show()
                    finish()
                }else{
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}

