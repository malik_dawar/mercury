package com.mercury.shoppingcart.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.GridView
import androidx.cardview.widget.CardView
import com.mercury.shoppingcart.pref.PrefManager
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

object UtilityFunctions {
    fun getFileDataFromDrawable(context: Context?, drawable: Drawable, bmp: Bitmap?): ByteArray {
        Log.d("getFileDataFromDrawable", "" + bmp)
        var bitmap: Bitmap? = null
        bitmap = bmp /*((BitmapDrawable) drawable).getBitmap();*/
                ?: (drawable as BitmapDrawable).bitmap
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream)
        Log.d("getFileDataFromDrawable", byteArrayOutputStream.toByteArray().toString() + "" + bmp)
        return byteArrayOutputStream.toByteArray()
    }

    fun validate(fields: Array<EditText>): Boolean {
        for (i in fields.indices) {
            val currentField = fields[i]
            if (currentField.text.toString().isEmpty()) {
                currentField.error = "Can't be Empty"
                currentField.requestFocus()
                return false
            } else {
                currentField.error = null
            }
        }
        return true
    }

    fun putBulkInPREF(mcontext: Context?, keys: Array<String?>, EditText: Array<EditText>) {
        for (i in keys.indices) {
            val value = EditText[i].text.toString()
            PrefManager(mcontext).putStringPref(keys[i], value)
        }
    }

    fun revertVisibility(cardView: CardView) {
        cardView.visibility = if (cardView.visibility == View.VISIBLE) View.GONE else View.VISIBLE
    }

    fun hideNshowCard(mCard: CardView) {
        if (mCard.visibility == View.GONE) mCard.visibility = View.VISIBLE else mCard.visibility = View.GONE
    }

    //function for listview in scrollview to mange hight
    fun setListViewHeightBasedOnChildren(gridView: GridView) {
        val listAdapter = gridView.adapter ?: return
        val desiredWidth = View.MeasureSpec.makeMeasureSpec(gridView.width, View.MeasureSpec.UNSPECIFIED)
        var totalHeight = 0
        var view: View? = null
        for (i in 0 until listAdapter.count) {
            view = listAdapter.getView(i, view, gridView)
            if (i == 0) view.layoutParams = ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT)
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
            totalHeight += view.measuredHeight
        }
        val params = gridView.layoutParams
        params.height = totalHeight + listAdapter.count
        gridView.layoutParams = params
    }

    fun setGridViewHeightBasedOnChildren(gridView: GridView, columns: Int) {
        val listAdapter = gridView.adapter
                ?: // pre-condition
                return
        var totalHeight = 0
        val items = listAdapter.count
        var rows = 0
        val listItem = listAdapter.getView(0, null, gridView)
        listItem.measure(0, 0)
        totalHeight = listItem.measuredHeight
        var x = 1f
        if (items > columns) {
            x = items / columns.toFloat()
            rows = (x + 1).toInt()
            totalHeight *= rows
        }
        val params = gridView.layoutParams
        params.height = totalHeight
        gridView.layoutParams = params
    }

    fun getDate(timeStamp: Long): String {
        return try {
            val sdf = SimpleDateFormat("MM/dd/yyyy")
            val netDate = Date(timeStamp)
            sdf.format(netDate)
        } catch (ex: Exception) {
            "xx"
        }
    }

    fun getFileDataFromDrawable(bitmap: Bitmap): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }
}