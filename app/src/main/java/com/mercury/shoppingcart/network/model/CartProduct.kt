package com.mercury.shoppingcart.network.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class CartProduct(@SerializedName("id")
                       @PrimaryKey(autoGenerate = true)
                       var id: Int = 0,
                       @ColumnInfo
                       var title: String? = "",
                       @ColumnInfo
                       var description: String? = "",
                       @ColumnInfo
                       var imgUrl: String? = "",
                       @ColumnInfo
                       var price: Int = 0,
                       @ColumnInfo
                       var categoryId: Int = 0,
                       @ColumnInfo
                       var typeId: Int = 0, // 0= cart , 1= favourite
                       @ColumnInfo
                       var quantity: Int = 0)