package com.mercury.shoppingcart.enums

enum class CategoriesEnum(val id : Int, val title: String)  {

    PosProduction(0, "POS Production"),
    BusinessStationery(1,"Business Stationery"),
    PromotionalGateway(2,"Promotional Gateway"),
    SignatureSolution(3,"Signature Solution"),
}